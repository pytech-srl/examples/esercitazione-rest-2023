import json
import requests

result = requests.get("http://127.0.0.1:8000/todo/api/activity_list/")

if result.status_code == 200:

    if result.headers['Content-Type'] == "application/json":

        data = json.loads(result.text)
        all_activity_list = data["activity_list"]

        for activity_list in all_activity_list:
            print(activity_list["name"])

            for activity in activity_list["activities"]:
                print("+", activity["name"])

            print()


