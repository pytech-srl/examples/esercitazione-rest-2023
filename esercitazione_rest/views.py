from django.http import HttpResponse

__all__ = ["home"]


def home(request):

    username = request.user.username if request.user.is_authenticated else "anonymous"

    response = "Welcome {user}!".format(
        user=username
    )

    return HttpResponse(response)
