from django.contrib import admin

from movies.models import Movie, StreamingChannel

admin.site.register(Movie)
admin.site.register(StreamingChannel)
