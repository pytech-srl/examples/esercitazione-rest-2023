from django.db import models

__all__ = [
    "Movie",
    "StreamingChannel",
]


class StreamingChannel(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return f"{self.pk}) {self.name}"


class Movie(models.Model):

    title = models.CharField(max_length=255)
    description = models.TextField()

    streaming_channel = models.ForeignKey(
        StreamingChannel,
        on_delete=models.CASCADE,
        related_name="streaming_channels"
    )

    def __str__(self):
        return f"{self.pk}) {self.title}"
