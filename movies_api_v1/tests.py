import random

from config import base
import requests
import unittest


class AuthenticationTest(unittest.TestCase):

    def test_registration_and_login(self):
        # Talking with her friends, Debora gets to know a new Website
        # that allows users to save their favourite films.
        # Going to check out its homepage, she notices it is necessary to
        # create a new account.
        random_number = random.randint(1000, 9999)
        username = f"debora{random_number}"
        password = "mypass123"
        registration_data = {
            "username": username,
            "email": f"{username}@asd.it",
            "password1": password,
            "password2": password
        }
        post_response = requests.post(
            f"{base.api_url}/dj-rest-auth/registration/",
            json=registration_data
        )
        # She hits enter and the account has been created.
        self.assertEqual(post_response.status_code, 204)

        login_data = {
            "username": username,
            "password": password
        }
        post_response = requests.post(
            f"{base.api_url}/dj-rest-auth/login/",
            json=login_data
        )
        self.assertEqual(post_response.status_code, 200)

        token = post_response.json().get("key")

        authorization_headers = {
            "Authorization": f"Token {token}"
        }

        response = requests.get(
            f"{base.api_url}/v1/movies/movies/",
            headers=authorization_headers
        )
        self.assertEqual(response.status_code, 200)


if __name__ == '__main__':
    unittest.main()
