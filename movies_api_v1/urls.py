from rest_framework.routers import SimpleRouter

from movies_api_v1.views import MovieAPIViewSet, StreamingChannelAPIViewSet

router = SimpleRouter()

router.register("movies", MovieAPIViewSet, basename="movies")
router.register("channels", StreamingChannelAPIViewSet, basename="channels")

urlpatterns = router.urls
