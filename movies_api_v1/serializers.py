from rest_framework import serializers

from movies.models import Movie, StreamingChannel

__all__ = [
    "MovieSerializer"
]


class MovieSerializer(serializers.ModelSerializer):
    class Meta:
        model = Movie
        fields = ("id", "title", "description", "streaming_channel")


class StreamingChannelSerializer(serializers.ModelSerializer):
    class Meta:
        model = StreamingChannel
        fields = ("id", "name")
