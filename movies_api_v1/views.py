from rest_framework import viewsets

from movies.models import Movie, StreamingChannel
from movies_api_v1.serializers import MovieSerializer, StreamingChannelSerializer

__all__ = [
    "MovieAPIViewSet",
    "StreamingChannelAPIViewSet"
]


class MovieAPIViewSet(viewsets.ModelViewSet):

    queryset = Movie.objects.all()
    serializer_class = MovieSerializer


class StreamingChannelAPIViewSet(viewsets.ModelViewSet):

    queryset = StreamingChannel.objects.all()
    serializer_class = StreamingChannelSerializer
